package fr.upem.partiel.part2.model

import Movie._
case class Movie(title: Title, director: Director, year: Year, views: Views, country: Country)

object Movie {

  case class Title(title : String)

  case class Director(fn: String, ln: String)

  case class Year(value: Int)

  case class Views(value: Long)

  trait Country

  object Country {

    final case object France extends Country

    final case object England extends Country

    final case object Italy extends Country

    final case object Germany extends Country

    final case object UnitedStates extends Country

  }

  def movie(title: Title, director: Director, year: Year, views: Views, country: Country): Movie = Movie(title, director, year, views, country)

  def title(s: String): Title = Title(s)

  def director(fn: String, ln: String): Director = Director(fn, ln)

  def year(value: Int): Year = Year(value)

  def views(value: Long): Views = Views(value)
}