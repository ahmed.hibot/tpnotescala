package fr.upem.partiel.part2.functions

import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie.{Director, Views}

object Functions {

  lazy val getDirectorNames: List[Movie] => List[String] = (movies : List[Movie] ) => movies.map(movie => s"${movie.director.fn} ${movie.director.ln}")

  lazy val viewMoreThan: Long => List[Movie] => List[Movie] = (views : Long) => (movies : List[Movie]) => movies.filter(movie => movie.views.value >= views)

  lazy val byDirector: List[Movie] => Map[Director, List[Movie]] = (movies : List[Movie]) => movies.groupBy(movie => movie.director)
}
